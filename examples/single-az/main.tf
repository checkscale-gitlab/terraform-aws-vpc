module "vpc" {
  source = "../../"

  vpc_name        = "stockbit"
  vpc_description = "vpc for stockbit assessment"

  vpc_cidr_block = "172.16.0.0/24"

  # subnets will be /25
  cidr_added_bits = 1

  subnet_availability_zones = ["ap-southeast-1a"]

  environment = "test"
}